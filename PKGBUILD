# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Feakster <feakster at posteo dot eu>
# Contributor: Felix Golatofski <contact at xdfr dot de>
# Contributor: Matthew Zilvar <mattzilvar at gmail dot com>
# Contributor: Térence Clastres <t dot clastres at gmail dot com>
# Modified PKGBUILD from https://aur.archlinux.org/packages/vivaldi/

pkgname=vivaldi
pkgver=4.3.2439.63
pkgrel=1
_pkgrel=1
pkgdesc='An advanced browser made with the power user in mind'
arch=('aarch64')
url="https://vivaldi.com"
license=('custom')
provides=('vivaldi' 'www-browser')
depends=('alsa-lib' 'desktop-file-utils' 'gtk3' 'hicolor-icon-theme' 'libcups' 'libxss' 'mesa' 'nss' 'shared-mime-info' 'ttf-font')
optdepends=('libnotify: native notifications')
options=('!emptydirs' '!strip')
install="$pkgname.install"
source=("https://downloads.vivaldi.com/stable/vivaldi-stable_${pkgver}-${_pkgrel}_arm64.deb"
        "eula.txt")
b2sums=('d139dabf0c6cccd397fa94f1854793f147120962ec7bbefb7e91ef7aabe904cc2e052ab634ecbffe87da6e576f8dbd1c52c7d49b0d36d69af8f3e1286e23b9d1'
        '1692e6980c125b0bf31936ea90b4aed87a1b35555201ae0cb075e4da88c5000a852a1c647b6b22d48631457ab7bf1208a59fc505378c11b6d0f26a9576d1ca1c')

prepare() {
    ## Extract Internals ##
    tar -xf data.tar.xz
}

package() {
    ## Copy Directory Structure ##
    cp --parents -a {opt,usr/bin,usr/share} "$pkgdir"

    ## SUID Sandbox ##
    chmod 4755 "$pkgdir"/opt/$pkgname/${pkgname}-sandbox
    
    ## Install Icons ##
    for res in 16 22 24 32 48 64 128 256; do
        install -dm0755 "$pkgdir"/usr/share/icons/hicolor/${res}x${res}/apps
        ln -fs /opt/$pkgname/product_logo_${res}.png "$pkgdir"/usr/share/icons/hicolor/${res}x${res}/apps/$pkgname.png
    done

    ## License and EULA ##
    install -dm0755 "$pkgdir"/usr/share/licenses/$pkgname
    ln -fs /opt/$pkgname/LICENSE.html "$pkgdir"/usr/share/licenses/$pkgname/LICENSE.html
    install -m644 "${srcdir}/eula.txt" "$pkgdir"/usr/share/licenses/$pkgname/
    
    ## Remove Debian/Widevine Directories/Files ##
    rm -rf "$pkgdir"/opt/$_pkgname/{cron,update-widevine,WidevineCdm}    
}
