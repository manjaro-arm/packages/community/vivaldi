## Written permission to distribute ##

Hi Dan,

Thank you for your email and offer to include Vivaldi in your repository.
Absolutely! We would love to see that.

One issue I want to mention though is that as you probably know, Google doesn’t offer arm64 version of Widevine for Linux.
Because of that, some video won’t play...

Please let me know if you need anything else from us.

Kind regards,
Tatsuki Tomita
